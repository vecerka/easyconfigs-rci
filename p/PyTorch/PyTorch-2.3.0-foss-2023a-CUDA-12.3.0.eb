name = 'PyTorch'
version = '2.3.0'
versionsuffix = '-CUDA-%(cudaver)s'

homepage = 'https://pytorch.org/'
description = """Tensors and Dynamic neural networks in Python with strong GPU acceleration.
PyTorch is a deep learning framework that puts Python first."""

toolchain = {'name': 'foss', 'version': '2023a'}

source_urls = [GITHUB_RELEASE]
sources = ['%(namelower)s-v%(version)s.tar.gz']
patches = [
    'PyTorch-1.7.0_disable-dev-shm-test.patch',
#    'PyTorch-1.11.1_skip-test_init_from_local_shards.patch',
    'PyTorch-1.12.1_add-hypothesis-suppression.patch',
    'PyTorch-1.12.1_fix-test_cpp_extensions_jit.patch',
    'PyTorch-1.12.1_fix-TestTorch.test_to.patch',
    'PyTorch-1.12.1_skip-test_round_robin.patch',
    'PyTorch-1.13.1_fix-gcc-12-warning-in-fbgemm.patch',
    'PyTorch-1.13.1_fix-protobuf-dependency.patch',
    'PyTorch-1.13.1_fix-warning-in-test-cpp-api.patch',
    'PyTorch-1.13.1_skip-failing-singular-grad-test.patch',
    'PyTorch-1.13.1_skip-tests-without-fbgemm.patch',
    'PyTorch-2.0.1_avoid-test_quantization-failures.patch',
    'PyTorch-2.0.1_fix-skip-decorators.patch',
#    'PyTorch-2.0.1_fix-ub-in-inductor-codegen.patch',
    'PyTorch-2.0.1_fix-vsx-loadu.patch',
#    'PyTorch-2.0.1_no-cuda-stubs-rpath.patch',
    'PyTorch-2.0.1_skip-failing-gradtest.patch',
    'PyTorch-2.0.1_skip-test_shuffle_reproducibility.patch',
    'PyTorch-2.0.1_skip-tests-skipped-in-subprocess.patch',
#    'PyTorch-2.1.0_disable-gcc12-warning.patch',
#    'PyTorch-2.1.0_fix-bufferoverflow-in-oneDNN.patch',
#    'PyTorch-2.1.0_fix-test_numpy_torch_operators.patch',
#    'PyTorch-2.1.0_fix-validationError-output-test.patch',
    'PyTorch-2.1.0_fix-vsx-vector-shift-functions.patch',
    'PyTorch-2.1.0_increase-tolerance-functorch-test_vmapvjpvjp.patch',
#    'PyTorch-2.1.0_remove-sparse-csr-nnz-overflow-test.patch',
#    'PyTorch-2.1.0_remove-test-requiring-online-access.patch',
#    'PyTorch-2.1.0_skip-diff-test-on-ppc.patch',
#    'PyTorch-2.1.0_skip-dynamo-test_predispatch.patch',
    'PyTorch-2.1.0_skip-test_jvp_linalg_det_singular.patch',
#    'PyTorch-2.1.0_skip-test_linear_fp32-without-MKL.patch',
#    'PyTorch-2.1.0_skip-test_wrap_bad.patch',
#    'PyTorch-2.1.2_fix-test_extension_backend-without-vectorization.patch',
#    'PyTorch-2.1.2_fix-test_memory_profiler.patch',
#    'PyTorch-2.1.2_fix-test_torchinductor-rounding.patch',
#    'PyTorch-2.1.2_fix-vsx-vector-abs.patch',
#    'PyTorch-2.1.2_fix-vsx-vector-div.patch',
#    'PyTorch-2.1.2_skip-cpu_repro-test-without-vectorization.patch',
#    'PyTorch-2.1.2_workaround_dynamo_failure_without_nnpack.patch',
]

osdependencies = [OS_PKG_IBVERBS_DEV]

builddependencies = [
    ('CMake', '3.26.3'),
    ('hypothesis', '6.82.0'),
    # For tests
    ('pytest-flakefinder', '1.1.0'),
    ('pytest-rerunfailures', '12.0'),
    ('pytest-shard', '0.1.2'),
]

dependencies = [
    ('Ninja', '1.11.1'),  # Required for JIT compilation of C++ extensions
    ('Python', '3.11.3'),
    ('Python-bundle-PyPI', '2023.06'),
    ('protobuf', '24.0'),
    ('protobuf-python', '4.24.0'),
    ('pybind11', '2.11.1'),
    ('SciPy-bundle', '2023.07'),
    ('PyYAML', '6.0'),
    ('MPFR', '4.2.0'),
    ('GMP', '6.2.1'),
    ('numactl', '2.0.16'),
    ('FFmpeg', '6.0'),
    ('Pillow', '10.0.0'),
    ('expecttest', '0.1.5'),
    ('typing-extensions','4.9.0'),
    ('networkx', '3.1'),
    ('CUDA', '12.3.0', '', SYSTEM),
    ('cuDNN', '8.9.7.29', '-CUDA-%(cudaver)s', SYSTEM),
    ('magma', '2.7.2', '-CUDA-%(cudaver)s'),
    ('NCCL', '2.19.4', '-CUDA-%(cudaver)s'),
    ('sympy', '1.12'),
    ('Z3', '4.12.2', '-Python-%(pyver)s'),
]

# default CUDA compute capabilities to use (override via --cuda-compute-capabilities)
cuda_compute_capabilities = ['8.0', '8.6', '8.9', '9.0']
use_pip = True

excluded_tests = {
    '': [
        # This test seems to take too long on NVIDIA Ampere at least.
        'distributed/test_distributed_spawn',
        # Broken on CUDA 11.6/11.7: https://github.com/pytorch/pytorch/issues/75375
        'distributions/test_constraints',
        # no xdoctest
        'doctests',
        # failing on broadwell
        # See https://github.com/easybuilders/easybuild-easyconfigs/issues/17712
        'test_native_mha',
        # intermittent failures on various systems
        # See https://github.com/easybuilders/easybuild-easyconfigs/issues/17712
        'distributed/rpc/test_tensorpipe_agent',
    ]
}

runtest = 'cd test && PYTHONUNBUFFERED=1 %(python)s run_test.py --continue-through-error  --verbose %(excluded_tests)s'

# Especially test_quantization has a few corner cases that are triggered by the random input values,
# those cannot be easily avoided, see https://github.com/pytorch/pytorch/issues/107030
# So allow a low number of tests to fail as the tests "usually" succeed
max_failed_tests = 2

tests = ['PyTorch-check-cpp-extension.py']

moduleclass = 'ai'
