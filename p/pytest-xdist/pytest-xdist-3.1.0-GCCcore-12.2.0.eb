easyblock = 'PythonBundle'

name = 'pytest-xdist'
version = '3.1.0'

homepage = 'https://github.com/pytest-dev/pytest-xdist'
description = """xdist: pytest distributed testing plugin

The pytest-xdist plugin extends pytest with some unique test execution modes:

    * test run parallelization: if you have multiple CPUs or hosts you
      can use those for a combined test run. This allows to speed up
      development or to use special resources of remote machines.

    * --looponfail: run your tests repeatedly in a subprocess. After
        each run pytest waits until a file in your project changes and
        then re-runs the previously failing tests. This is repeated
        until all tests pass after which again a full run is
        performed.

    * Multi-Platform coverage: you can specify different Python
      interpreters or different platforms and run tests in parallel on
      all of them.

Before running tests remotely, pytest efficiently “rsyncs” your
program source code to the remote place. All test results are reported
back and displayed to your local terminal. You may specify different
Python versions and interpreters."""

toolchain = {'name': 'GCCcore', 'version': '12.2.0'}

builddependencies = [('binutils', '2.39')]

dependencies = [
    ('Python', '3.10.8'),
]

use_pip = True
sanity_pip_check = True

exts_list = [
    ('apipkg', '1.5', {
        'checksums': ['37228cda29411948b422fae072f57e31d3396d2ee1c9783775980ee9c9990af6'],
    }),
    ('execnet', '1.9.0', {
        'checksums': ['8f694f3ba9cc92cab508b152dcfe322153975c29bda272e2fd7f3f00f36e47c5'],
    }),
    ('pytest-forked', '1.6.0', {
        'checksums': ['4dafd46a9a600f65d822b8f605133ecf5b3e1941ebb3588e943b4e3eb71a5a3f'],
    }),
    (name, version, {
        'modulename': 'xdist',
        'checksums': ['40fdb8f3544921c5dfcd486ac080ce22870e71d82ced6d2e78fa97c2addd480c'],
    }),
]

sanity_check_paths = {
    'files': [],
    'dirs': ['lib/python%(pyshortver)s/site-packages'],
}

moduleclass = 'tools'
