easyblock = 'PythonBundle'

name = 'TensorFlow'
version = '2.11.0'

homepage = 'https://www.tensorflow.org/'
description = "An open-source software library for Machine Intelligence"

toolchain = {'name': 'foss', 'version': '2022a'}
toolchainopts = {'pic': True}

builddependencies = [
    ('Bazel', '5.1.1'),
    ('protobuf', '3.19.4'),
    # git 2.x required, see also https://github.com/tensorflow/tensorflow/issues/29053
    ('git', '2.36.0', '-nodocs'),
    ('pybind11', '2.9.2'),
    ('UnZip', '6.0'),
    ('LLVM', '14.0.3'),  # for debugging with llvm-symbolizer, to be removed
]
dependencies = [
    ('Python', '3.10.4'),
    ('tensorboard',version),
    ('h5py', '3.7.0'),
    ('cURL', '7.83.0'),
    ('double-conversion', '3.2.0'),
#    ('flatbuffers', '1.12.1'),
    ('giflib', '5.2.1'),
    ('hwloc', '2.7.1'),
    ('ICU', '71.1'),
    ('JsonCpp', '1.9.5'),
    ('libjpeg-turbo', '2.1.3'),
    ('LMDB', '0.9.29'),
    ('NASM', '2.15.05'),
    ('nsync', '1.25.0'),
    ('SQLite', '3.38.3'),
    ('protobuf-python', '3.19.4'),
    ('flatbuffers-python', '2.0'),
    ('libpng', '1.6.37'),
    ('snappy', '1.1.9'),
    ('zlib', '1.2.12'),
    ('networkx', '2.8.4'),  # required for pythran
]

use_pip = True
sanity_pip_check = False

# Dependencies created and updated using findPythonDeps.sh:
# https://gist.github.com/Flamefire/49426e502cd8983757bd01a08a10ae0d
exts_list = [
    ('wrapt', '1.14.1', {
        'checksums': ['380a85cf89e0e69b7cfbe2ea9f765f004ff419f34194018a6827ac0e3edfed4d'],
    }),
    ('termcolor', '1.1.0', {
        'checksums': ['1d6d69ce66211143803fbc56652b41d73b4a400a2891d7bf7a1cdf4c02de613b'],
    }),
    ('tensorflow-io-gcs-filesystem', '0.27.0', {
        'source_tmpl': 'tensorflow_io_gcs_filesystem-0.27.0-cp310-cp310-manylinux_2_12_x86_64.manylinux2010_x86_64.whl',
        'checksums': ['b3a0ebfeac11507f6fc96162b8b22010b7d715bb0848311e54ef18d88f07014a'],
    }),
    ('tensorflow-estimator', version , {
        'source_tmpl': 'tensorflow_estimator-2.11.0-py2.py3-none-any.whl',
        'checksums': ['ea3b64acfff3d9a244f06178c9bdedcbdd3f125b67d0888dba8229498d06468b'],
    }),
    ('opt-einsum', '3.3.0', {
        'source_tmpl': 'opt_einsum-3.3.0-py3-none-any.whl',
        'checksums': ['2455e59e3947d3c275477df7f5205b30635e266fe6dc300e3d9f9646bfcea147'],
    }),
    ('libclang', '14.0.6', {
	'modulename': 'clang',
        'checksums': ['9052a8284d8846984f6fa826b1d7460a66d3b23a486d782633b42b6e3b418789'],
    }),
    ('Keras-Preprocessing', '1.1.2', {
        'source_tmpl': 'Keras_Preprocessing-1.1.2-py2.py3-none-any.whl',
        'checksums': ['7b82029b130ff61cc99b55f3bd27427df4838576838c5b2f65940e4fcec99a7b'],
    }),
    ('keras', version, {
        'source_tmpl': 'keras-2.11.0-py2.py3-none-any.whl',
        'checksums': ['38c6fff0ea9a8b06a2717736565c92a73c8cd9b1c239e7125ccb188b7848f65e'],
    }),
    ('google-pasta', '0.2.0', {
 	'modulename': 'pasta',
        'checksums': ['c9f2c8dfc8f96d0d5808299920721be30c9eec37f2389f28904f454565c8a16e'],
    }),
    ('astunparse', '1.6.3', {
        'checksums': ['5ad93a8456f0d084c3456d059fd9a92cce667963232cbf763eac3bc5b7940872'],
    }),
    ('gast', '0.4.0', {
        'checksums': ['40feb7b8b8434785585ab224d1568b857edb18297e5a3047f1ba012bc83b42c1'],
    }),
    ('beniget', '0.3.0', {
        'checksums': ['062c893be9cdf87c3144fb15041cce4d81c67107c1591952cd45fdce789a0ff1'],
    }),
    ('pythran', '0.9.11', {
        'checksums': ['a317f91e2aade9f6550dc3bf40b5caeb45b7e012daf27e2b3e4ad928edb01667'],
    }),
    (name, version, {
        'patches': [
            'TensorFlow-2.1.0_fix-cuda-build.patch',
#            'TensorFlow-2.4.0_add-ldl.patch',
#            'TensorFlow-2.4.0_dont-use-var-lock.patch',
#            'TensorFlow-2.5.0_add-support-for-large-core-systems.patch',
#            'TensorFlow-2.5.0_disable-avx512-extensions.patch',
#            'TensorFlow-2.5.0-fix-alias-violation-in-absl.patch',
#            'TensorFlow-2.5.0_fix-crash-on-shutdown.patch',
#            'TensorFlow-2.7.1_fix_cpu_count.patch',
#            'TensorFlow-2.7.1_remove-duplicate-gpu-tests.patch',
#            'TensorFlow-2.8.4_remove-libclang-and-io-gcs-deps.patch',
#            'TensorFlow-2.7.1_fix_cpu_count.patch',
#	     'TensorFlow-2.9.1_fix-include-def.patch',
       ],
        'source_tmpl': 'v%(version)s.tar.gz',
        'source_urls': ['https://github.com/tensorflow/tensorflow/archive/'],
#        'test_script': 'TensorFlow-2.x_mnist-test.py',
#        'test_tag_filters_cpu': '-gpu,-tpu,-no_cuda_on_cpu_tap,-no_pip,-no_oss,-oss_serial,-benchmark-test,-v1only',
#        'test_tag_filters_gpu': 'gpu,-no_gpu,-nogpu,-gpu_cupti,-no_cuda11,-no_pip,-no_oss,-oss_serial,-benchmark-test,-v1only',
#        'test_targets': [
#            '//tensorflow/core/...',
#            '-//tensorflow/core:example_java_proto',
#            '-//tensorflow/core/example:example_protos_closure',
#            '//tensorflow/cc/...',
#            '//tensorflow/c/...',
#            '//tensorflow/python/...',
#            '-//tensorflow/c/eager:c_api_test_gpu',
#            '-//tensorflow/c/eager:c_api_distributed_test',
#            '-//tensorflow/c/eager:c_api_distributed_test_gpu',
#            '-//tensorflow/c/eager:c_api_cluster_test_gpu',
#            '-//tensorflow/c/eager:c_api_remote_function_test_gpu',
#            '-//tensorflow/c/eager:c_api_remote_test_gpu',
#            '-//tensorflow/core/common_runtime:collective_param_resolver_local_test',
#            '-//tensorflow/core/common_runtime:mkl_layout_pass_test',
#            '-//tensorflow/core/kernels/mkl:mkl_fused_ops_test',
#            '-//tensorflow/core/kernels/mkl:mkl_fused_batch_norm_op_test',
#            '-//tensorflow/core/ir/importexport/tests/roundtrip/...',
#        ],
#        'testopts': "--test_timeout=3600 --test_size_filters=small",
#        'testopts_gpu': "--test_timeout=3600 --test_size_filters=small --run_under=//tensorflow/tools/ci_build/gpu_build:parallel_gpu_execute",
        'with_xla': True,
    }),
]

moduleclass = 'lib'
