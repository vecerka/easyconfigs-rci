easyblock = 'PythonBundle'

name = 'TensorFlow'
version = '2.12.0'

homepage = 'https://www.tensorflow.org/'
description = "An open-source software library for Machine Intelligence"

toolchain = {'name': 'foss', 'version': '2022b'}
toolchainopts = {'pic': True}

builddependencies = [
    ('Bazel', '5.3.0'),
    ('protobuf', '23.0'),
    # git 2.x required, see also https://github.com/tensorflow/tensorflow/issues/29053
    ('git', '2.38.1', '-nodocs'),
    ('pybind11', '2.10.3'),
    ('UnZip', '6.0'),
    ('LLVM', '15.0.5'),  # for debugging with llvm-symbolizer, to be removed
]
dependencies = [
    ('Python', '3.10.8'),
    ('tensorboard','2.12.3'),
    ('h5py', '3.8.0'),
    ('cURL', '7.86.0'),
    ('double-conversion', '3.2.1'),
  #  ('flatbuffers', '23.1.4'),
    ('giflib', '5.2.1'),
    ('hwloc', '2.8.0'),
    ('ICU', '72.1'),
    ('JsonCpp', '1.9.5'),
    ('libjpeg-turbo', '2.1.4'),
    ('LMDB', '0.9.29'),
    ('NASM', '2.15.05'),
    ('nsync', '1.25.0'),
    ('SQLite', '3.39.4'),
    ('protobuf-python', '4.23.0'),
    ('flatbuffers-python', '22.12.6'),
    ('libpng', '1.6.38'),
    ('snappy', '1.1.9'),
    ('zlib', '1.2.12'),
    ('networkx', '3.0'),  # required for pythran
]

use_pip = True
sanity_pip_check = False

# Dependencies created and updated using findPythonDeps.sh:
# https://gist.github.com/Flamefire/49426e502cd8983757bd01a08a10ae0d
exts_list = [
    ('wrapt', '1.14.1', {
        'checksums': ['380a85cf89e0e69b7cfbe2ea9f765f004ff419f34194018a6827ac0e3edfed4d'],
    }),
    ('termcolor', '1.1.0', {
        'checksums': ['1d6d69ce66211143803fbc56652b41d73b4a400a2891d7bf7a1cdf4c02de613b'],
    }),
    ('tensorflow-io-gcs-filesystem', '0.32.0', {
        'source_tmpl': 'tensorflow_io_gcs_filesystem-0.32.0-cp310-cp310-manylinux_2_12_x86_64.manylinux2010_x86_64.whl',
        'checksums': ['045d51bba586390d0545fcd8a18727d62b175eb142f6f4c6d719d39de40774cd'],
    }),
    ('tensorflow-estimator', version , {
        'source_tmpl': 'tensorflow_estimator-2.12.0-py2.py3-none-any.whl',
        'checksums': ['59b191bead4883822de3d63ac02ace11a83bfe6c10d64d0c4dfde75a50e60ca1'],
    }),
    ('opt-einsum', '3.3.0', {
        'source_tmpl': 'opt_einsum-3.3.0-py3-none-any.whl',
        'checksums': ['2455e59e3947d3c275477df7f5205b30635e266fe6dc300e3d9f9646bfcea147'],
    }),
    ('libclang', '14.0.6', {
	'modulename': 'clang',
        'checksums': ['9052a8284d8846984f6fa826b1d7460a66d3b23a486d782633b42b6e3b418789'],
    }),
    ('Keras-Preprocessing', '1.1.2', {
        'source_tmpl': 'Keras_Preprocessing-1.1.2-py2.py3-none-any.whl',
        'checksums': ['7b82029b130ff61cc99b55f3bd27427df4838576838c5b2f65940e4fcec99a7b'],
    }),
    ('keras', version, {
        'source_tmpl': 'keras-2.12.0-py2.py3-none-any.whl',
        'checksums': ['35c39534011e909645fb93515452e98e1a0ce23727b55d4918b9c58b2308c15e'],
    }),
    ('google-pasta', '0.2.0', {
 	'modulename': 'pasta',
        'checksums': ['c9f2c8dfc8f96d0d5808299920721be30c9eec37f2389f28904f454565c8a16e'],
    }),
    ('astunparse', '1.6.3', {
        'checksums': ['5ad93a8456f0d084c3456d059fd9a92cce667963232cbf763eac3bc5b7940872'],
    }),
    ('gast', '0.4.0', {
        'checksums': ['40feb7b8b8434785585ab224d1568b857edb18297e5a3047f1ba012bc83b42c1'],
    }),
    ('beniget', '0.3.0', {
        'checksums': ['062c893be9cdf87c3144fb15041cce4d81c67107c1591952cd45fdce789a0ff1'],
    }),
    ('pythran', '0.9.11', {
        'checksums': ['a317f91e2aade9f6550dc3bf40b5caeb45b7e012daf27e2b3e4ad928edb01667'],
    }),
    (name, version, {
     'source_tmpl': 'tensorflow-2.12.0-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl',
        'checksums': ['357d9d2851188a8d27ee195345b4d175cad970150d1344ba9d9fcc4bf2b68336'],
    }),
]

moduleclass = 'lib'
