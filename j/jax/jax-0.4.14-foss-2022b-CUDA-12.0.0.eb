# This file is an EasyBuild reciPY as per https://github.com/easybuilders/easybuild
# Author: Denis Kristak
# Updated by: Alex Domingo (Vrije Universiteit Brussel)
easyblock = 'PythonBundle'

name = 'jax'
version = '0.4.14'
versionsuffix = '-CUDA-%(cudaver)s'

homepage = 'https://pypi.python.org/pypi/jax'
description = """Composable transformations of Python+NumPy programs:
differentiate, vectorize, JIT to GPU/TPU, and more"""

toolchain = {'name': 'foss', 'version': '2022b'}

builddependencies = [
    ('Bazel', '5.3.0'),
    ('pytest-xdist', '3.1.0'),
    # git 2.x required to fetch repository 'io_bazel_rules_docker'
    ('git', '2.38.1', '-nodocs'),
    ('matplotlib', '3.7.0'),  # required for tests/lobpcg_test.py
]

dependencies = [
    ('CUDA', '12.0.0', '', SYSTEM),
    ('cuDNN', '8.8.0.121', versionsuffix, SYSTEM),
    ('NCCL', '2.16.2', versionsuffix),
    ('Python', '3.10.8'),
    ('SciPy-bundle', '2023.02'),
    ('flatbuffers-python', '22.12.6'),
]

# downloading TensorFlow tarball to avoid that Bazel downloads it during the build
# note: this *must* be the exact same commit as used in WORKSPACE
local_tf_commit = '43e9d313548ded301fa54f25a4192d3bcb123330'
local_tf_dir = 'tensorflow-%s' % local_tf_commit
local_tf_builddir = '%(builddir)s/' + local_tf_dir

# replace remote TensorFlow repository with the local one from EB
local_jax_prebuildopts = "sed -i -f jaxlib_local-tensorflow-repo.sed WORKSPACE && "
local_jax_prebuildopts += "sed -i 's|EB_TF_REPOPATH|%s|' WORKSPACE && " % local_tf_builddir

use_pip = True

default_easyblock = 'PythonPackage'
default_component_specs = {
    'sources': [SOURCE_TAR_GZ],
    'source_urls': [PYPI_SOURCE],
    'start_dir': '%(name)s-%(version)s',
    'use_pip': True,
    'sanity_pip_check': True,
    'download_dep_fail': True,
}

components = [
    ('absl-py', '1.4.0', {
        'options': {'modulename': 'absl'},
        'checksums': ['d2c244d01048ba476e7c080bd2c6df5e141d211de80223460d5b3b8a2a58433d'],
    }),
    ('jaxlib', version, {
        'sources': [
            '%(name)s-v%(version)s.tar.gz',
            {
                'download_filename': '%s.tar.gz' % local_tf_commit,
                'filename': 'tensorflow-%s.tar.gz' % local_tf_commit,
            }
        ],
        'source_urls': [
            'https://github.com/google/jax/archive/',
            'https://github.com/tensorflow/tensorflow/archive/'
        ],
        'patches': [
            ('jaxlib_local-tensorflow-repo.sed', '.'),
            ('TensorFlow-2.7.0_cuda-noncanonical-include-paths.patch', '../' + local_tf_dir),
        ],
        'start_dir': 'jax-jaxlib-v%(version)s',
        'prebuildopts': local_jax_prebuildopts,
    }),
]

exts_list = [
    ('opt_einsum', '3.3.0', {
        'checksums': ['59f6475f77bbc37dcf7cd748519c0ec60722e91e63ca114e68821c0c54a46549'],
    }),
    ('flit_core', '3.9.0', {
        'checksums': ['72ad266176c4a3fcfab5f2930d76896059851240570ce9a98733b658cb786eba'],
    }),

    ('etils', '1.3.0', {
        'checksums': ['0a695ec45a982ae7c9deb437f1f251346d88b43ca59be67e961f61fe8bc8cae4'],
    }),
    ('ml_dtypes', '0.1.0', {
	'source_tmpl': 'ml_dtypes-0.1.0-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl',
        'checksums': ['dee8ea629b8e3e20c6649852c1b9deacfa13384ab9337f2c9e717e401d102f23'],
    }),
    (name, version, {
        'patches': [
            'jax-0.3.23_relax-testPoly5-tolerance.patch',
            'jax-0.4.4_cusparse.patch',
        ],
        'source_tmpl': '%(name)s-v%(version)s.tar.gz',
        'source_urls': ['https://github.com/google/jax/archive/'],
    }),
]

sanity_pip_check = True

moduleclass = 'tools'
