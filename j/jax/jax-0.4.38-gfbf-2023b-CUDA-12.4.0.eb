# This file is an EasyBuild reciPY as per https://github.com/easybuilders/easybuild
# Author: Denis Kristak
# Updated by: Alex Domingo (Vrije Universiteit Brussel)
# Updated by: Pavel Tománek (INUITS)
# Updated by: Thomas Hoffmann (EMBL Heidelberg)
easyblock = 'PythonBundle'

name = 'jax'
version = '0.4.38'
versionsuffix = '-CUDA-%(cudaver)s'

homepage = 'https://jax.readthedocs.io/'
description = """Composable transformations of Python+NumPy programs:
differentiate, vectorize, JIT to GPU/TPU, and more"""

toolchain = {'name': 'gfbf', 'version': '2023b'}
cuda_compute_capabilities = ["5.0", "6.0", "6.1", "7.0", "7.5", "8.0", "8.6", "9.0"]

builddependencies = [
    ('Bazel', '6.5.0'), 
    ('pytest-xdist', '3.6.1'),
    ('git', '2.42.0'),  # bazel uses git to fetch repositories
    ('matplotlib', '3.8.2'),  # required for tests/lobpcg_test.py
    ('poetry', '1.7.1'),
    ('Clang', '17.0.6')
]

dependencies = [
    ('CUDA', '12.4.0', '', SYSTEM),  # 12.6.2 ?
    ('cuDNN', '9.5.0.50', versionsuffix, SYSTEM),
    ('NCCL', '2.20.5', versionsuffix),
    ('Python', '3.11.5'),
    ('SciPy-bundle', '2023.11'),  # 2024.11 ?
    ('absl-py', '2.1.0'),
    ('flatbuffers-python', '23.5.26'),
    ('ml_dtypes', '0.5.0'),
    ('zlib', '1.2.13'),
    ('pybind11', '2.12.0'),
]

# downloading xla and other tarballs to avoid that Bazel downloads it during the build
local_extract_cmd = 'mkdir -p %(builddir)s/archives && cp %s %(builddir)s/archives'
# note: following commits *must* be the exact same onces used upstream
# XLA_COMMIT from jax-jaxlib: third_party/xla/workspace.bzl
local_xla_commit = '20a482597b7dd3067b26ca382b88084ee5a21cf7'

# Use sources downloaded by EasyBuild
_jaxlib_buildopts = '--bazel_options="--distdir=%(builddir)s/archives" '
# Use dependencies from EasyBuild
_jaxlib_buildopts += '--bazel_options="--action_env=TF_SYSTEM_LIBS=pybind11" '
_jaxlib_buildopts += '--bazel_options="--action_env=CPATH=$EBROOTPYBIND11/include:$EBROOTCUDA/extras/CUPTI/include" '
# Avoid warning (treated as error) in upb/table.c
_jaxlib_buildopts += '--bazel_options="--copt=-Wno-maybe-uninitialized" '  # TODO: still required?
# _jaxlib_buildopts += '--nouse_clang '  #TODO: avoid clang (?)
_jaxlib_buildopts += '--cuda_version=%(cudaver)s '
_jaxlib_buildopts += '--python_bin_path=$EBROOTPYTHON/bin/python3 '
# Do not use hermetic CUDA/cuDNN/NCCL: (requires action_env=CPATH=$EBROOTCUDA/extras/CUPTI/include";
# requires patch of external/xla/xla/tsl/cuda/cupti_stub.cc and jaxlib/gpu/vendor.h (#include <cupti.h>): 
_jaxlib_buildopts += """--bazel_options=--repo_env=LOCAL_CUDNN_PATH="$EBROOTCUDNN" """
_jaxlib_buildopts += """--bazel_options=--repo_env=LOCAL_NCCL_PATH="$EBROOTNCCL" """
_jaxlib_buildopts += """--bazel_options=--repo_env=LOCAL_CUDA_PATH="$EBROOTCUDA" """
_jaxlib_buildopts += """--bazel_options="--copt=-Ithird_party/gpus/cuda/extras/CUPTI/include" """

# get rid of .devDate versionsuffix:  TODO: find a better way
# _no_devtag = """ export JAX_RELEASE && export JAXLIB_RELEASE && """  does not work (?)
_no_devtag = """ sed -i "s/version=__version__/version='%(version)s'/g" setup.py && """
_jaxlib_buildopts += """--bazel_options="--action_env=JAXLIB_RELEASE=1" """  # required?

components = [
    ('jaxlib', version, {
        'sources': [
            {
                'source_urls': ['https://github.com/google/jax/archive/'],
                'filename': 'jax-v%(version)s.tar.gz',
            },
            {
                'source_urls': ['https://github.com/openxla/xla/archive'],
                'download_filename': '%s.tar.gz' % local_xla_commit,
                'filename': 'xla-%s.tar.gz' % local_xla_commit[:8],
                'extract_cmd': local_extract_cmd,
            },
        ],
        'patches': [
            'jax-0.4.34_easyblock_compat.patch',
            'jax-0.4.34_fix-pybind11-systemlib_cupti.patch',
            'jax-0.4.34_version.patch',
        ],
        'start_dir': 'jax-jax-v%(version)s',
        'buildopts': _jaxlib_buildopts,
    }),
]

# Some tests require an isolated run:  TODO: still required?
local_isolated_tests = [
    'tests/host_callback_test.py::HostCallbackTapTest::test_tap_scan_custom_jvp',
    'tests/host_callback_test.py::HostCallbackTapTest::test_tap_transforms_doc',
    'tests/lax_scipy_special_functions_test.py::LaxScipySpcialFunctionsTest' +
    '::testScipySpecialFun_gammainc_s_2x1x4_float32_float32',
]
# deliberately not testing in parallel, as that results in (additional) failing tests;
# use XLA_PYTHON_CLIENT_ALLOCATOR=platform to allocate and deallocate GPU memory during testing,
# see https://github.com/google/jax/issues/7323 and
# https://github.com/google/jax/blob/main/docs/gpu_memory_allocation.rst;
# use CUDA_VISIBLE_DEVICES=0 to avoid failing tests on systems with multiple GPUs;
# use NVIDIA_TF32_OVERRIDE=0 to avoid loosing numerical precision by disabling TF32 Tensor Cores;
local_test_exports = [
    "NVIDIA_TF32_OVERRIDE=0",
    "CUDA_VISIBLE_DEVICES=0",
    "XLA_PYTHON_CLIENT_ALLOCATOR=platform",
    "JAX_ENABLE_X64=true",
]
local_test = ''.join(['export %s;' % x for x in local_test_exports])
# run all tests at once except for local_isolated_tests:
local_test += "pytest -vv tests %s && " % ' '.join(['--deselect %s' % x for x in local_isolated_tests])
# run remaining local_isolated_tests separately: 
local_test += ' && '.join(['pytest -vv %s' % x for x in local_isolated_tests])

use_pip = True

exts_list = [
    (name, version, {
        'patches': ['jax-0.4.34_version.patch'],
        'preinstallopts': _no_devtag,
        'runtest': False,
        'source_tmpl': '%(name)s-v%(version)s.tar.gz',
        'source_urls': ['https://github.com/google/jax/archive/'],
    }),
]

sanity_pip_check = True

moduleclass = 'ai'
